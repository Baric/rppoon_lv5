﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Zad1_2
{
    class Program
    {
        private static readonly IEnumerable<IShipable> list;

        static void Main(string[] args)
        {
            Box box = new Box("Product Box");
            Product product = new Product("Watch", 13.37, 4);
            Product pro = new Product("Earphones", 55, 2);
            box.Add(product);
            box.Add(pro);


            ShippingService price = new ShippingService(4);
            double w = 0;
            foreach (IShipable ship in list)
            {

                w += ship.Weight;

                Console.WriteLine(ship.Description());

            }

            Console.WriteLine(price.ToString() + price.Price(w) + " HRK");

        }
    }
}