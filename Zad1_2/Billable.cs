﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad1_2
{
    interface IBillable
    {
        double Price { get; }
        string Description(int depth = 0);
    }
}
