﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad1_2
{
    class ShippingService
    {
        private double jm;

        public ShippingService(double jm) { this.jm = jm; }

        public double Price(double weight)
        {
            return jm * weight;
        }

        public override string ToString()
        {
            return "Total shipping price: ";
        }
    }
}