﻿using System;

namespace Zad3_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data1 = new Dataset("File.txt");
            User user1 = User.GenerateUser("Anthony");
            User user2 = User.GenerateUser("Marcus");
            User user3 = User.GenerateUser("James");
            User user4 = User.GenerateUser("Fred");
            User user5 = User.GenerateUser("Scott");
            ProtectionProxyDataset proxy1 = new ProtectionProxyDataset(user1);
            ProtectionProxyDataset proxy2 = new ProtectionProxyDataset(user2);
            ProtectionProxyDataset proxy3 = new ProtectionProxyDataset(user3);
            ProtectionProxyDataset proxy4 = new ProtectionProxyDataset(user4);
            ProtectionProxyDataset proxy5 = new ProtectionProxyDataset(user5);
            VirtualProxyDataset proxy6 = new VirtualProxyDataset("File.txt");
            DataConsolePrinter printer = new DataConsolePrinter();
            printer.Print(proxy1);
            Console.WriteLine();
            printer.Print(proxy2);
            Console.WriteLine();
            printer.Print(proxy3);
            Console.WriteLine();
            printer.Print(proxy4);
            Console.WriteLine();
            printer.Print(proxy5);
            Console.WriteLine();
            Console.WriteLine("Virutal: \n");
            printer.Print(proxy6);

        }

    }
}