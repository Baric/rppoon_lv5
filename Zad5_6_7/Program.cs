﻿using System;

namespace Zad5_6_7
{
    class Program
    {
        static void Main(string[] args)
        {

            ITheme Light = new LightTheme();
            ITheme Dark = new DarkTheme();


            ReminderNote note1 = new ReminderNote("Buy paint", Dark);
            ReminderNote note2 = new ReminderNote("Paint it, Black", Dark);
            GroupNote note3 = new GroupNote("List of movies to watch: ", Light);
            note3.insert("1917");
            note3.insert("Parasite");
            note3.Show();
            note3.delete(1);
            note3.Show();

            Notebook notes = new Notebook(Dark);
            notes.AddNote(note1);
            notes.AddNote(note2);
            notes.AddNote(note3);
            notes.Display();

        }
    }
}
